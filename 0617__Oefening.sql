use modernways;
create view AuteursBoekenRatings
as 
select auteursboeken.Auteur, auteursboeken.Titel, gemiddelderatings.Ratings
from auteursboeken
inner join gemiddeldeRatings on auteursboeken.boeken_Id = gemiddeldeRatings.boeken_Id