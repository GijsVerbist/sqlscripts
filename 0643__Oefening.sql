USE `aptunes`;
DROP procedure IF EXISTS `CreateAndReleaseAlbum`;
DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `CreateAndReleaseAlbum`(in Titel varchar(100), in bands_Id int)
BEGIN
start transaction;
insert into albums(
titel 
)
values(
Titel
);
insert into albumreleases(
Bands_Id,
Albums_Id
) 
values(
bands_Id,
LAST_INSERT_ID()
);
commit;
END$$
DELIMITER ;