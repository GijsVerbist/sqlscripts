USE `aptunes`;
DROP procedure IF EXISTS `GetAlbumDuration`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `GetAlbumDuration` (in albumID int, out totalDuration int)
BEGIN

declare totalDuration smallint unsigned default 0;
declare songDuration tinyint unsigned default 0;
declare ok bool default false;
declare lengthOfAlbums cursor for select lengte from liedjes where albumID = liedjes.Albums_Id;
declare continue handler for not found set ok = true;
open lengthOfAlbums;

getLength: loop
fetch lengthOfAlbums into songDuration;
if ok = true then
leave fetchloop;
end if;
SET totalDuration = totalDuration + songDuration;
select totalDuration;
end loop getLength;
close lengthOfAlbums;
END$$

DELIMITER ;

