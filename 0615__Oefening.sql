use modernways;
alter view auteursboeken
as
select concat(Voornaam, ' ', Familienaam) as 'Auteur', titel, boeken.Id as 'boeken_Id' from boeken
inner join publicaties on boeken.Id = publicaties.boeken_Id
inner join personen on publicaties.personen_Id = personen.Id
