use modernways;
select voornaam
from studenten
where Voornaam = any(select Voornaam from personeelsleden)
having Voornaam = any(select Voornaam from directieleden)

//of
//use modernways;
//select distinct Voornaam
//from studenten
//where voornaam in (select voornaam from personeelsleden) and voornaam in (select voornaam from directieleden)