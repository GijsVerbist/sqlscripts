use modernways;
create view AuteursBoeken
as
select concat(Voornaam, ' ', Familienaam) as 'Auteur', titel from boeken
inner join publicaties on boeken.Id = publicaties.boeken_Id
inner join personen on publicaties.personen_Id = personen.Id
