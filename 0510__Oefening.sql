USE ModernWays;
INSERT INTO Liedjes (
titel, 
artiest,
album,
jaar
)
VALUES( 'stairway to heaven', 'Led Zeppelin', ' Led Zeppelin IV', '1971'),
('Good Enough', 'Molly Tuttle', 'Rise', '2017'),
('Outrage for the Execution of Willie McGee', 'Goodnight, Texas ', 'Conductor', '2018'),
('They Lie', 'Layla Zoe', 'The Lily','2013'),
('It Ain''t You', 'Danielle Nicole', 'Wolf Den', '2015'),
('unchained', 'Van Halen', 'Fair Warning', '1981');