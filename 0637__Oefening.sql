-- tabel 'personen' maken om zowel auteurs en ontleners te kunnen weergeven
use ModernWays;
drop table if exists Personen;
create table Personen (
    Voornaam varchar(255) not null,
    Familienaam varchar(255) not null
);

-- tabel 'Personen' invullen en ervoor zorgen dat er geen dubbels zijn adhv 'distinct'
insert into Personen (Voornaam, Familienaam)
   select distinct Voornaam, Familienaam from Boeken;
   
-- nakijken ofdat de vorige code wel correct is. 
select Voornaam, Familienaam from Personen
    order by Voornaam, Familienaam;
    
-- andere kolommen en primary key toevoegen aan de tabel 

alter table Personen add (
   Id int auto_increment primary key,
   AanspreekTitel varchar(30) null,
   Straat varchar(80) null,
   Huisnummer varchar (5) null,
   Stad varchar (50) null,
   Commentaar varchar (100) null,
   Biografie varchar(400) null);
   
   -- nakijken of we hebben wat we willen
select * from Personen order by Familienaam, Voornaam;

-- een foreign key toevoegen 
alter table Boeken add Personen_Id int null;

-- bekijken hoe we de tabellen moet linken door ze naast elkaar te zetten
select Boeken.Voornaam,
   Boeken.Familienaam,
   Boeken.Personen_Id,
   Personen.Voornaam,
   Personen.Familienaam,
   Personen.Id
from Boeken cross join Personen
where Boeken.Voornaam = Personen.Voornaam and
    Boeken.Familienaam = Personen.Familienaam;
    
    -- de foreign key maken en aangeven dat de waarde van beide tabellen overeenkomen
SET SQL_SAFE_UPDATES = 0;
update Boeken cross join Personen
    set Boeken.Personen_Id = Personen.Id
where Boeken.Voornaam = Personen.Voornaam and
    Boeken.Familienaam = Personen.Familienaam;
    SET SQL_SAFE_UPDATES = 1;
    
    -- de foreign keys not null maken zodat het verplicht is
alter table Boeken change Personen_Id Personen_Id int not null;

-- checken ofdat het klopt
select Voornaam, Familienaam, Personen_Id from Boeken;

-- we zagen zonet dat er dubbele kolommen zijn met dezelfde id's. Die worden hier verwijdert
alter table Boeken drop column Voornaam,
    drop column Familienaam;
    
    -- de foreign key vervolledigen
alter table Boeken add constraint fk_Boeken_Personen
   foreign key(Personen_Id) references Personen(Id);
   
	select * from boeken;
