USE `aptunes`;
DROP procedure IF EXISTS `MockAlbumRelease`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `MockAlbumRelease` ()
BEGIN
declare numberOfAlbums int default 0;
declare numberOfBands int default 0;
declare randomAlbumId int default 0;
declare randomBandId int default 0;
select count(Id) into numberOfAlbums from albums;
select count(Id) into numberOfBands from bands;
set randomAlbumId = FLOOR(RAND() * numberOfAlbums) + 1;
set randomBandId = FLOOR(RAND() * numberOfBands) + 1;
if randomAlbumId != randomAlbumId in (select Albums_Id from albumreleases) and randomBandId != randomBandId in (select Bands_Id from albumreleases) then
insert into albumreleases(
Bands_Id,
Albums_Id
)
values(
randomBandId,
randomAlbumId
);
end if;
END$$

DELIMITER ;