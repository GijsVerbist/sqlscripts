USE `aptunes`;
DROP procedure IF EXISTS `GetAlbumDuration2`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `GetAlbumDuration2` (in albumID int, out totalDuration int)
SQL SECURITY invoker
BEGIN

declare something int default 0;
declare totalDuration smallint unsigned default 0;
declare songDuration tinyint unsigned default 0;
declare albumDuration tinyint unsigned default 0;
declare lengthOfAlbums cursor for select lengte from liedjes where albumID = liedjes.Albums_Id;

open lengthOfAlbums;

getLength: loop
fetch lengthOfAlbums into songDuration;
SET totalDuration = totalDuration + songDuration;
select totalDuration;
end loop getLength;
close lengthOfAlbums;
END$$

DELIMITER ;

