USE `aptunes`;
DROP procedure IF EXISTS `NumberOfGenres `;
DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `NumberOfGenres`(out totalGenres tinyint)
BEGIN
select count(*)
into totalGenres
from genres;
END$$
DELIMITER ;
