use aptunes;
select count(distinct Voornaam)
from muzikanten;

select count(distinct left(Voornaam, 9))
from muzikanten; -- 651 

select count(distinct Familienaam)
from muzikanten;

select count(distinct left(Familienaam, 9))
from muzikanten; -- 988

create index VoornaamFamilienaamIdx
on muzikanten(Voornaam(9), Familienaam(9))

