USE `aptunes`;
DROP procedure IF EXISTS `MockAlbumReleases`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `MockAlbumReleases` (in extraReleases int)
BEGIN
declare counter int default 0;
declare succes bool;
repeat
call MockAlbumReleaseWithSuccess(succes);
if succes = 1 then 
set counter = counter + 1;
end if;
until counter = extraReleases
end repeat;
END$$

DELIMITER ;

