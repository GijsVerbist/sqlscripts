-- toont alle geldige combinaties van liedjestitels en genres

-- create index naamIdx
 -- on Genres(Naam);
select Titel, Naam
from Liedjesgenres inner join Liedjes
on Liedjesgenres.Liedjes_Id = Liedjes.Id
inner join Genres
on Liedjesgenres.Genres_Id = Genres.Id
where Naam = 'Rock';