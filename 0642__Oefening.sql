USE `aptunes`;
DROP procedure IF EXISTS `CleanupOldMemberships`;
DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `CleanupOldMemberships`(in dateInput date, out numbersGone int)
BEGIN
start transaction;
select count(*) into numbersGone
from lidmaatschappen
where lidmaatschappen.Einddatum is not null and Lidmaatschappen.Einddatum < dateInput;
set sql_safe_updates = 0;
delete from Lidmaatschappen
where Lidmaatschappen.Einddatum is not null and Lidmaatschappen.Einddatum < dateInput;
set sql_safe_updates = 1;
commit;
END$$
DELIMITER ;