USE `aptunes`;
DROP procedure IF EXISTS `DemonstrateHandlerOrder`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `DemonstrateHandlerOrder` ()
BEGIN
declare randomNumber int default 0;
declare continue handler for sqlstate '45002'
begin
select 'State 45002 opgevangen. Geen probleem' ;
end;
declare continue handler for sqlexception 
begin
select 'Een algemene fout opgevangen.';
end;
set randomNumber = FLOOR(RAND() * 3) + 1;
if randomNumber = 1 then
SIGNAL SQLSTATE '45001';
elseif randomNumber = 2 then 
SIGNAL SQLSTATE '45002';
else
SIGNAL SQLSTATE '45003';
end if;
END$$

DELIMITER ;

