USE `aptunes`;
DROP procedure IF EXISTS `GetLiedjes`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `GetLiedjes`(in songName varchar(50))
BEGIN
select titel from liedjes
where titel like concat('%' ,songName, '%');
END$$
DELIMITER ;