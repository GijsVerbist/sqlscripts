USE `aptunes`;
DROP procedure IF EXISTS `MockAlbumReleasesLoop`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `MockAlbumReleasesLoop` (in extraReleases int)
BEGIN
declare i int default 0;
declare success bool;
looping: loop
call MockAlbumReleaseWithSuccess(success);
if success = 1 then 
set i = i + 1;
end if;
if i = extraReleases then
leave looping;
end if;
end loop;
END$$

DELIMITER ;

