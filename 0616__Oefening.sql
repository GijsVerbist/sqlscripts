use modernways;
create view gemiddeldeRatings
as
select boeken_Id, AVG(Rating) as 'Ratings'
from reviews
group by Boeken_Id