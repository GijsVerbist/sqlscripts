USE `aptunes`;
DROP procedure IF EXISTS `DangerousInsertAlbumreleases`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `DangerousInsertAlbumreleases` ()
BEGIN
declare signaling int default 0;
declare numberOfAlbums int default 0;
declare numberOfBands int default 0;
declare randomAlbumId1 int default 0;
declare randomBandId1 int default 0;
declare randomAlbumId2 int default 0;
declare randomBandId2 int default 0;
declare randomAlbumId3 int default 0;
declare randomBandId3 int default 0;
declare counter int default 0;
DECLARE exit HANDLER FOR sqlexception
begin
rollback;
select 'Nieuwe releases konden niet worden toegevoegd';
end;
start transaction;
select count(Id) into numberOfAlbums from albums;
select count(Id) into numberOfBands from bands;
set randomAlbumId1 = FLOOR(RAND() * numberOfAlbums) + 1;
set randomBandId1 = FLOOR(RAND() * numberOfBands) + 1;
set randomAlbumId2 = FLOOR(RAND() * numberOfAlbums) + 1;
set randomBandId2 = FLOOR(RAND() * numberOfBands) + 1;
set randomAlbumId3 = FLOOR(RAND() * numberOfAlbums) + 1;
set randomBandId3 = FLOOR(RAND() * numberOfBands) + 1;
set signaling = FLOOR(RAND() * 3) + 1;

insert into albumreleases(
Bands_Id,
Albums_Id
)
values(
randomBandId1,
randomAlbumId1
);
set counter = 1;
insert into albumreleases(
Bands_Id,
Albums_Id
)
values(
randomBandId2,
randomAlbumId2
);
set counter = 2;
set signaling = 2;
if signaling = 1 then
SIGNAL SQLSTATE '45000';
end if;
insert into albumreleases(
Bands_Id,
Albums_Id
)
values(
randomBandId3,
randomAlbumId3
);
set counter = 3;
if counter < 3 then
SIGNAL SQLSTATE '45000';
end if;

commit;
END$$

DELIMITER ;

